package com.example.demo.controller;

import com.example.demo.model.FlightSearch;
import com.example.demo.repository.FlightSearchRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;



@RefreshScope
@RestController
@RequestMapping("flight-search")
@Tag(name = "flightSearch", description = "The FlightSearch API")
public class FlightSearchController {
	protected static Logger logger = LoggerFactory.getLogger(FlightSearchController.class.getName());
	@Autowired
    private FlightSearchRepository flightSearchRepo;


	@Autowired
	private Environment env;

	@Value("${server.port}")
	private int serverPort;


	@LoadBalanced
	@GetMapping("/hello")
	public ResponseEntity<String> getHello(Model model) {

		System.out.println("serverPort : "+serverPort);
		return new ResponseEntity<String>( env.getProperty("msg"), HttpStatus.OK);
	}

	 @Operation(summary = "Add a new Flight Details", description = "", tags = { "flightSearch" })
	 @ApiResponses(value = {
			 @ApiResponse(responseCode = "201", description = "Flight added",
					 content = @Content(schema = @Schema(implementation = FlightSearch.class))),
			 @ApiResponse(responseCode = "400", description = "Invalid input"),
			 @ApiResponse(responseCode = "409", description = "Flight already exists") })

	 @RequestMapping(value="/createFlightEntry", method = RequestMethod.POST)
		public ResponseEntity<FlightSearch> createFlightEntry(@Parameter(description="Flight details to add. Cannot null or empty.",
			required=true, schema=@Schema(implementation = FlightSearch.class))
												   @Valid @RequestBody FlightSearch flightSearch){
		try{
			return new ResponseEntity<>(flightSearchRepo.save(flightSearch), HttpStatus.CREATED);
		}catch(Exception ex){
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@Operation(summary = "Get Flight details by id" , description = "", tags = { "flightSearch" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Flight details received by Id",
					content = @Content(mediaType = "application/json",schema = @Schema(implementation = FlightSearch.class))),
			@ApiResponse(responseCode = "404", description = "NOT FOUND", content = @Content),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content) })
	@RequestMapping(value="/flightSearch/{id}", method = RequestMethod.GET)
	public ResponseEntity<FlightSearch> getFlightByFlightId(@PathVariable Long id) {
		Optional<FlightSearch> flightSearch = flightSearchRepo.findById(id);

		if (flightSearch.isPresent()) {
			return new ResponseEntity<>(flightSearch.get(), HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@Operation(summary = "Get all Flights" , description = "", tags = { "flightSearch" })
	@RequestMapping(value="/getAllFlights", method = RequestMethod.GET)
	public ResponseEntity<List<FlightSearch>> getAllFlights() {
		try {
			List<FlightSearch> list = flightSearchRepo.findAll();
			if (list.isEmpty() || list.size() == 0) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(list, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Operation(summary = "Get Flight availability by id" , description = "", tags = { "flightSearch" })
	@RequestMapping(value="/getAvailabilityById/{id}", method = RequestMethod.GET)
	public int getAvailabilityById(@PathVariable long id) {
		try {
			Optional<FlightSearch> searchedFlightDetails = flightSearchRepo.findById(id);
			if (!searchedFlightDetails.isPresent()) {
				return 0;
			}
			return searchedFlightDetails.get().getAvailability();
		} catch (Exception e) {
			return 0;
		}
	}

	@Operation(summary = "Increase Flight availability by id and availability count" , description = "", tags = { "flightSearch" })
	@PutMapping("/increaseFlightAvailability/{id}/{availability}")
	public ResponseEntity<FlightSearch> increaseFlightAvailability(@PathVariable long id, @PathVariable int availability) {
		try {
			int currentAvaliability = getAvailabilityById(id);
			ResponseEntity<FlightSearch> searchedFlightDetails =getFlightByFlightId(id);
			searchedFlightDetails.getBody().setAvailability(availability + currentAvaliability);
			return new ResponseEntity<>(flightSearchRepo.save(searchedFlightDetails.getBody()), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Operation(summary = "Decrease Flight availability by id and availability count" , description = "", tags = { "flightSearch" })
	@PutMapping("/decreaseFlightAvailability/{id}/{availability}")
	public ResponseEntity<FlightSearch> decreaseFlightAvailability(@PathVariable long id, @PathVariable int availability) {
		try {
			int currentAvaliability = getAvailabilityById(id);
			ResponseEntity<FlightSearch> searchedFlightDetails =getFlightByFlightId(id);
			searchedFlightDetails.getBody().setAvailability(currentAvaliability - availability);
			return new ResponseEntity<>(flightSearchRepo.save(searchedFlightDetails.getBody()), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


}
