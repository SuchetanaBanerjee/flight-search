package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.FlightSearch;

@Repository
public interface FlightSearchRepository extends JpaRepository<FlightSearch,Long>{

}
