package com.example.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "FlightSearch")
public class FlightSearch {

	private Long id;
	private Long flightId;
    private String flightName;
    private int availability;
    private Date start_date;
    
    public FlightSearch() {
    	
    }
	public FlightSearch(Long flightId,String flightName, int availability, Date start_date) {
    	this.flightId = flightId;
		this.flightName = flightName;
		this.availability = availability;
		this.start_date = start_date;
	}

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "FlightId")
	//@NotEmpty(message = "Flight Id Is Required")
	public Long getFlightId() {	return flightId;	}

	public void setFlightId(Long flightId) {this.flightId = flightId;}
	
	@Column(name = "Flight_Name")
	//@NotEmpty(message = "Flight Name Is Required")
	public String getFlightName() {
		return flightName;
	}
	public void setFlightName(String flightName) {
		this.flightName = flightName;
	}
	
	@Column(name = "Flight_availability")
	@NotNull(message = "Flight availability Is Required")
	public int getAvailability() {	return availability; }
	public void setAvailability(int availability) {
		this.availability = availability;
	}
	
	@Column(name = "Flight_StartDate")
	//@NotEmpty(message = "Flight Start Date Is Required")
	public Date getStart_date() {
		return start_date;
	}
	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	@Override
	public String toString() {
		return "FlightSearch{" +
				"id=" + id +
				", flightId=" + flightId +
				", flightName='" + flightName + '\'' +
				", availability=" + availability +
				", start_date=" + start_date +
				'}';
	}
}
